%global _empty_manifest_terminate_build 0
Name:		python-autopep8
Version:	1.5.7
Release:	1
Summary:	A tool that automatically formats Python code to conform to the PEP 8 style guide
License:	Expat License
URL:		https://github.com/hhatto/autopep8
Source0:	https://files.pythonhosted.org/packages/77/63/e88f70a614c21c617df0ee3c4752fe7fb66653cba851301d3bcaee4b00ea/autopep8-1.5.7.tar.gz
BuildArch:	noarch


%description
A tool that automatically formats Python code to conform to the PEP 8 style guide

%package -n python3-autopep8
Summary:	A tool that automatically formats Python code to conform to the PEP 8 style guide
Provides:	python-autopep8
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
Requires:	python3-pycodestyle
Requires:	python3-toml
%description -n python3-autopep8
A tool that automatically formats Python code to conform to the PEP 8 style guide

%package help
Summary:	Development documents and examples for autopep8
Provides:	python3-autopep8-doc
%description help
A tool that automatically formats Python code to conform to the PEP 8 style guide

%prep
%autosetup -n autopep8-1.5.7

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-autopep8 -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Jul 23 2021 Xu Jin <jinxu@kylinos.cn> - 1.5.7-1
- Update package to 1.5.7

* Sat Jul 18 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
